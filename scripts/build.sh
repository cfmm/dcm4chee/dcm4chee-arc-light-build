#!/bin/sh
set -e

BUILD_FLAG=
ARTIFACT_LABEL=
YARN_DEBUG=
PROJECT_DIR=$(pwd)

if [ "x$1" = "xsecure" ]; then
    BUILD_FLAG='-D secure=all'
    ARTIFACT_LABEL=secure
fi

DEPLOYMENT_DIR=${PROJECT_DIR}/docker/dcm4chee-arc/deployments${ARTIFACT_LABEL:+-${ARTIFACT_LABEL}}

# Make the deployments directory
rm -rf ${DEPLOYMENT_DIR}
mkdir -p ${DEPLOYMENT_DIR}

# Build the custom ROOT.war
cd ROOT
jar -cf ${DEPLOYMENT_DIR}}/ROOT.war ./*
cd ${PROJECT_DIR}

if [ "x$CI" = "xtrue" ]; then
  FILENAME=dcm4chee-arc-${DCM4CHEE_VERSION}-psql${ARTIFACT_LABEL:+-${ARTIFACT_LABEL}}
  URL=${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/dcm4chee-arc/${ARC_SHA}/${FILENAME}.zip
  response=$(curl --write-out '%{http_code}' --silent --header "JOB-TOKEN: $CI_JOB_TOKEN" --location ${URL} --output ${FILENAME}.zip)
  if [ "x$response" = "x200" ]; then
    # Built package for the submodule already exist and were downloaded
    jar xvf ${FILENAME}.zip
    cp ${FILENAME}/deploy/* ${DEPLOYMENT_DIR}
    exit 0
  else
    echo "Download of ${URL} failed with $response"
  fi
fi

if [ "x$2" = "xdebug" ]; then
    YARN_DEBUG='-D yarn.run=dev'
fi

# Update the dcm4chee-arc-light images with custom images
cd ROOT
cp img/* ${PROJECT_DIR}/dcm4chee-arc-light/dcm4chee-arc-ui2/src/assets/img/

# Build the required dcm4chee-arc-lang module
cd ${PROJECT_DIR}/dcm4chee-arc-lang
./mvnw ${MAVEN_CLI_OPTS} install

# Build the dcm4chee-arc-light project
cd ${PROJECT_DIR}/dcm4chee-arc-light

# Clean ui2 since images may have changed and maven is not smart enough to detect the change
./mvnw -pl dcm4chee-arc-ui2 clean

# Build dcm4chee-arc-light
./mvnw ${MAVEN_CLI_OPTS} install -D db=psql ${BUILD_FLAG} ${YARN_DEBUG}

# Install the dcm4chee-arc-ear for docker deployment
./mvnw ${MAVEN_CLI_OPTS} dependency:copy -pl dcm4chee-arc-ear -D db=psql ${BUILD_FLAG} -Dartifact='org.dcm4che.dcm4chee-arc:dcm4chee-arc-ear:${project.version}:ear:psql'${ARTIFACT_LABEL:+-${ARTIFACT_LABEL}} -DoutputDirectory="${DEPLOYMENT_DIR}"
./mvnw ${MAVEN_CLI_OPTS} dependency:copy -pl dcm4chee-arc-ui2 -D db=psql ${BUILD_FLAG} -Dartifact='org.dcm4che.dcm4chee-arc:dcm4chee-arc-ui2:${project.version}:war'${ARTIFACT_LABEL:+:${ARTIFACT_LABEL}} -DoutputDirectory="${DEPLOYMENT_DIR}"
