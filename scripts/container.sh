#!/bin/sh
set -e

cd docker
export ARTIFACT_LABEL=secure
docker compose build --pull arc
