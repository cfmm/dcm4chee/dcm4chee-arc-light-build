#!/bin/bash

if [[ "x$CI_COMMIT_TAG" == "x$DCM4CHEE_VERSION" ]]; then
  curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file dcm4chee-arc-light/dcm4chee-arc-assembly/target/dcm4chee-arc-${DCM4CHEE_VERSION}-psql-secure.zip ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/dcm4chee-arc/${DCM4CHEE_VERSION}/dcm4chee-arc-${DCM4CHEE_VERSION}-psql-secure.zip
  curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file dcm4chee-arc-light/dcm4chee-arc-assembly/target/dcm4chee-arc-${DCM4CHEE_VERSION}-psql.zip ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/dcm4chee-arc/${DCM4CHEE_VERSION}/dcm4chee-arc-${DCM4CHEE_VERSION}-psql.zip
fi
