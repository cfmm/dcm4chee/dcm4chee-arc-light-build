import pytest
from cfmm_keycloak.fixtures import _make
from .config import Arc


@pytest.fixture(scope="session")
def arc_config_service():
    return _make(Arc, timeout=30)
