from ldap3 import MODIFY_REPLACE, MODIFY_DELETE
from ldap3.core.exceptions import LDAPNoSuchAttributeResult, LDAPNoSuchObjectResult

from cfmm_keycloak.ldap import LDAP
from cfmm_keycloak.configure import env_config
from cfmm_keycloak.ad import ADConfigure
from types import SimpleNamespace


class Arc(LDAP):
    def __init__(self, config: SimpleNamespace | None = None):
        super().__init__(config=config or env_config.arc)

    def webapp(self):
        conn = self.connection
        base = f"dicomDeviceName={self.config.device},cn=Devices,cn=DICOM Configuration,{self.config.base}"
        changes = {"dcmProperty": [(MODIFY_REPLACE, [f"role={self.config.admin_role}"])]}
        for app_name in (
            "AS_RECEIVED",
            "IOCM_EXPIRED",
            "IOCM_PAT_SAFETY",
            "IOCM_QUALITY",
            "IOCM_REGULAR_USE",
            "IOCM_WRONG_MWL",
        ):
            conn.modify(f"dcmWebAppName={app_name},{base}", changes)
            conn.modify(f"dcmWebAppName={app_name}-WADO,{base}", changes)

    def ui(self):
        conn = self.connection
        device = f"dicomDeviceName={self.config.device},cn=Devices,cn=DICOM Configuration,{self.config.base}"
        base = f"dcmuiConfigName=default,{device}"
        changes = {"dcmAcceptedUserRole": [(MODIFY_REPLACE, [self.config.admin_role])]}
        for permission in (
            "Action - Data Exchange Monitoring Export All Actions",
            "Action - Device Configuration",
            "Action - Monitoring - Diff Monitor All Action",
            "Action - Monitoring - External Retrieve All Actions",
            "Action - Monitoring Queues All Actions",
            "Action - Monitoring Queues Single Actions",
            "Action - Retrieve - Single Action",
            "Action - Studies - Copy Merge Move",
            "Action - Studies - More Function",
            "Action - Studies - MWL",
            "Action - Studies - Patient",
            "Menu - Configuration",
            "Menu - Monitoring",
            "Tab - Config Control",
            "Tab - Configuration - Web App List",
            "Tab - Configuration Ae List",
            "Tab - Configuration Devices",
            "Tab - Configuration Hl7 Applications",
            "Tab - Monitoring Associations",
            "Tab - Monitoring External Retrieve",
            "Tab - Monitoring Metrics",
            "Tab - Monitoring Queues",
            "Tab - Monitoring Storage Commitments",
            "Tab - Monitoring Storage Systems",
            "Tab - Study - Diff",
            "Tab - Study - Mpps",
            "Tab - Study - Mwl",
            "Tab - Study - Uwl",
        ):
            dn = f"dcmuiPermissionName={permission},{base}"
            try:
                conn.modify(dn, changes)
            except LDAPNoSuchObjectResult:
                raise

        # Change the permission name to have something more useful than (1)
        for permission in ("Action - Studies - Series", "Action - Studies - Study", "Action - Studies - Instance"):
            dn = f"dcmuiPermissionName={permission}(1),{base}"
            rel_dn = f"dcmuiPermissionName={permission}(Admin)"
            try:
                conn.modify_dn(dn, rel_dn)
            except LDAPNoSuchObjectResult:
                continue

        changes["dcmuiActionParam"] = [(MODIFY_REPLACE, ["reject", "restore"])]
        for permission in ("Action - Studies - Series(Admin)",):
            conn.modify(f"dcmuiPermissionName={permission},{base}", changes)

        changes = {"dcmAcceptedUserRole": [(MODIFY_REPLACE, [self.config.admin_role, self.config.user_role])]}
        for permission in (
            "Action - Monitoring Export Single Action",
            "Action - Studies - Count",
            "Action - Studies - Download study",
            "Action - Studies - Open Viewer",
            "Action - Studies - Size",
            "Action - Studies - Verify Storage Commitment",
            "Menu - Navigation",
            "Tab - Monitoring Export",
            "Tab - Study - Patient",
            "Tab - Study - Study",
        ):
            conn.modify(f"dcmuiPermissionName={permission},{base}", changes)

        changes["dcmuiActionParam"] = [(MODIFY_REPLACE, ["export"])]
        for permission in (
            "Action - Monitoring Export Single Action",
            "Action - Studies - Count",
            "Action - Studies - Download study",
            "Action - Studies - Open Viewer",
            "Action - Studies - Size",
            "Action - Studies - Studies",
        ):
            conn.modify(f"dcmuiPermissionName={permission},{base}", changes)

        changes["dcmuiActionParam"] = [(MODIFY_REPLACE, ["visible", "export"])]
        for permission in ("Action - Studies - Series",):
            conn.modify(f"dcmuiPermissionName={permission},{base}", changes)

        name = "Action - Studies - Instance(Admin)"
        self.add(
            f"dcmuiPermissionName={name},{base}",
            ["dcmuiPermission"],
            {
                "dcmuiAction": "action-studies-instance",
                "dcmuiPermissionName": name,
                "dcmAcceptedUserRole": self.config.admin_role,
                "dcmuiActionParam": ["reject", "restore"],
            },
        )

        permission = "Action - Studies - Studies(Admin)"
        self.add(
            f"dcmuiPermissionName={permission},{base}",
            ["dcmuiPermission"],
            {
                "dcmuiAction": "action-studies-study",
                "dcmuiPermissionName": permission,
                "dcmAcceptedUserRole": self.config.admin_role,
                "dcmuiActionParam": ["delete", "edit", "reject", "restore", "upload"],
            },
        )

        name = "Action - Studies - Instance(Upload)"
        self.add(
            f"dcmuiPermissionName={permission},{base}",
            ["dcmuiPermission"],
            {
                "dcmuiAction": "action-studies-instance",
                "dcmuiPermissionName": permission,
                "dcmAcceptedUserRole": self.config.upload_role,
                "dcmuiActionParam": ["upload"],
            },
        )

        name = "Action - Studies - Studies(Upload)"
        self.add(
            f"dcmuiPermissionName={permission},{base}",
            ["dcmuiPermission"],
            {
                "dcmuiAction": "action-studies-study",
                "dcmuiPermissionName": permission,
                "dcmAcceptedUserRole": self.config.upload_role,
                "dcmuiActionParam": ["upload"],
            },
        )

        name = "Action - Studies - Seriess(Upload)"
        self.add(
            f"dcmuiPermissionName={permission},{base}",
            ["dcmuiPermission"],
            {
                "dcmuiAction": "action-studies-series",
                "dcmuiPermissionName": permission,
                "dcmAcceptedUserRole": self.config.upload_role,
                "dcmuiActionParam": ["upload"],
            },
        )

    def ae_titles(self):
        conn = self.connection

        dn = (
            f"dicomAETitle={self.config.aetitle}TLS,cn=Unique AE Titles Registry,"
            f"cn=DICOM Configuration,{self.config.base}"
        )
        self.add(dn, ["dicomUniqueAETitle"], {})

        device = f"dicomDeviceName={self.config.device},cn=Devices,cn=DICOM Configuration,{self.config.base}"
        dn = f"dicomAETitle={self.config.aetitle},{device}"
        changes = {
            "dicomNetworkConnectionReference": [
                (
                    MODIFY_DELETE,
                    [
                        f"cn=dicom-tls,dicomDeviceName={self.config.device},"
                        f"cn=Devices,cn=DICOM Configuration,{self.config.base}"
                    ],
                )
            ]
        }
        try:
            conn.modify(dn, changes)
        except LDAPNoSuchAttributeResult:
            pass

        dn = f"dicomAETitle={self.config.aetitle},{device}"
        changes = {"dcmUserIdentityNegotiation": [(MODIFY_REPLACE, ["SUPPORTS"])]}
        conn.modify(dn, changes)

        dn = f"dicomAETitle={self.config.aetitle}TLS,{device}"
        self.add(
            dn,
            ["dicomNetworkAE", "dcmNetworkAE", "dcmArchiveNetworkAE"],
            {
                "dcmObjectStorageID": "fs1",
                "dicomDescription": "TLS AET",
                "dicomAETitle": f"{self.config.aetitle}TLS",
                "dicomNetworkConnectionReference": f"cn=dicom-tls,{device}",
                "dicomAssociationAcceptor": True,
                "dcmQueryRetrieveViewID": "hideRejected",
                "dcmUserIdentityNegotiation": "REQUIRED",
                "dicomAssociationInitiator": True,
            },
        )

        self.copy_entries(
            source_ou=f"dicomAETitle={self.config.aetitle},{device}",
            destination_ou=f"dicomAETitle={self.config.aetitle}TLS,{device}",
            search_filter="(objectClass=dicomTransferCapability)",
        )

        dn = f"cn=Verification SOP Class SCP,dicomAETitle={self.config.aetitle}TLS,{device}"
        self.add(
            dn,
            ["dicomTransferCapability", "dcmTransferCapability"],
            {
                "dicomSOPClass": "1.2.840.10008.1.1",
                "dicomTransferRole": "SCP",
                "cn": "Verification SOP Class SCP",
                "dicomTransferSyntax": "1.2.840.10008.1.2",
            },
        )

        dn = f"cn=Verification SOP Class SCU,dicomAETitle={self.config.aetitle}TLS,{device}"
        self.add(
            dn,
            ["dicomTransferCapability", "dcmTransferCapability"],
            {
                "dicomSOPClass": "1.2.840.10008.1.1",
                "dicomTransferRole": "SCU",
                "cn": "Verification SOP Class SCU",
                "dicomTransferSyntax": "1.2.840.10008.1.2",
            },
        )

    def network(self, keycloak_uri, realm_name):
        conn = self.connection

        device = f"dicomDeviceName={self.config.device},cn=Devices,cn=DICOM Configuration,{self.config.base}"
        dn = f"dicomAETitle={self.config.aetitle},{device}"
        changes = {"dcmAcceptedCallingAETitle": [(MODIFY_REPLACE, ["MASTER", "CLIENT"])]}
        conn.modify(dn, changes)

        changes = {
            "dcmUserIdentityNegotiationRole": [(MODIFY_REPLACE, [self.config.user_role])],
            "dcmUserIdentityNegotiationKeycloakClientID": [(MODIFY_REPLACE, ["dcm4chee-arc-ui"])],
            "dcmUserIdentityNegotiation": [(MODIFY_REPLACE, ["REQUIRED"])],
            "dcmAllowDeleteStudyPermanently": [(MODIFY_REPLACE, ["ALWAYS"])],
        }
        conn.modify(device, changes)

        dn = f"cn=dicom,{device}"
        changes = {"dicomPort": [(MODIFY_REPLACE, [11114])]}
        conn.modify(dn, changes)

        dn = f"cn=dicom-tls,{device}"
        changes = {
            "dicomPort": [(MODIFY_REPLACE, [11112])],
            "dcmTLSNeedClientAuth": [(MODIFY_REPLACE, [False])],
            "dicomTLSCipherSuite": [
                (
                    MODIFY_REPLACE,
                    [
                        "SSL_RSA_WITH_3DES_EDE_CBC_SHA",
                        "TLS_RSA_WITH_AES_128_CBC_SHA",
                        "TLS_DHE_RSA_WITH_AES_128_GCM_SHA256",
                        "TLS_DHE_RSA_WITH_AES_256_GCM_SHA384",
                        "TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256",
                        "TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384",
                    ],
                )
            ],
        }
        conn.modify(dn, changes)

        self.add(
            dn=f"dcmKeycloakClientID=dcm4chee-arc-ui,{device}",
            object_class=["dcmKeycloakClient"],
            attributes={
                "dcmKeycloakClientID": "dcm4chee-arc-ui",
                "dcmKeycloakRealm": realm_name,
                "dcmURI": keycloak_uri,
                "dcmKeycloakGrantType": "password",
                "dcmTLSAllowAnyHostname": True,
                "dcmTLSDisableTrustManager": False,
            },
        )

    def access_control(self, samba: ADConfigure):
        device = f"dicomDeviceName={self.config.device},cn=Devices,cn=DICOM Configuration,{self.config.base}"

        # By default, assign the gidNumber of the Datacare group with the lowest priority
        result = samba.find_group("Datacare")
        if result is not None:
            access_control_id = result["gidNumber"]
            entity = "DatacareDefault"
            self.add(
                dn=f"cn={entity},{device}",
                object_class=["dcmStoreAccessControlIDRule"],
                attributes={
                    "cn": entity,
                    "dcmStoreAccessControlID": access_control_id,
                    "dcmRulePriority": 0,
                },
            )

        for entity, attrs in samba.config.dicom_access_control.items():
            result = samba.find_group(entity)
            if result is not None:
                access_control_id = result["gidNumber"]
                data = entity.split("-")
                if len(data) < 2:
                    condition = fr"StudyDescription=(?i)\Q{data[0]}\E[\^ ].*"
                    rule_priority = 5
                else:
                    condition = fr"StudyDescription=(?i)(\Q{data[0]}^{data[1]}\E|\Q{data[0]}\E[\^ ]\Q{data[1]}\E)$"
                    rule_priority = 10

                self.add(
                    dn=f"cn={entity},{device}",
                    object_class=["dcmStoreAccessControlIDRule"],
                    attributes={
                        "cn": entity,
                        "dcmStoreAccessControlID": access_control_id,
                        "dcmProperty": condition,
                        "dcmRulePriority": rule_priority,
                    },
                )
