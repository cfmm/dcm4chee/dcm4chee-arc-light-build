import collections
import json
import os
import shutil
import socket
import warnings
from typing import Callable

import requests
from urllib3.exceptions import InsecureRequestWarning
from keycloak import KeycloakOpenID

class ArcRESTClient(object):
    def __init__(
        self,
        arc_url="https://arc.dcm4chee/dcm4chee-arc",
        auth_url="http://keycloak.dcm4chee/",
        client_id="dcm4chee-arc-rs",
        realm_name="dcm4che",
        aetitle="DCM4CHEE",
        client_secret_key=None,
    ):
        self.arc_url = arc_url
        self._arc_wado_url = f"{self.arc_url}/aets/{aetitle}"
        if auth_url[-1] != "/":
            auth_url += "/"

        self._keycloak_openid = KeycloakOpenID(
            server_url=auth_url,
            client_id=client_id,
            realm_name=realm_name,
            verify=False,
            client_secret_key=client_secret_key,
        )
        self._active_token = None

    def as_user(self, username: str, password: str):
        self._active_token = self._keycloak_openid.token(username=username, password=password)

    def _process_request(self, request: Callable, *args, **kwargs):
        if self._active_token is None:
            self.as_user("admin", "secret")

        kwargs.setdefault('headers', {})
        kwargs["headers"].update({"Authorization": f"Bearer {self._active_token['access_token']}"})
        kwargs.setdefault('verify', False)
        with warnings.catch_warnings():
            warnings.filterwarnings("ignore", category=InsecureRequestWarning)
            resp = request(*args, **kwargs)

            # Try refreshing the token
            if resp.status_code in (401, 403):
                self._active_token = self._keycloak_openid.refresh_token(self._active_token['refresh_token'])
                kwargs["headers"].update({"Authorization": f"Bearer {self._active_token['access_token']}"})
                resp = request(*args, **kwargs)

        return resp

    def reload(self):
        resp = self._process_request(requests.post, f"{self.arc_url}/ctrl/reload")
        return resp.status_code == 204

    def remove_study(self, study_iuid):
        resp = self._process_request(requests.delete, f"{self._arc_wado_url}/rs/studies/{study_iuid}")
        return resp.status_code == 204

    def wado_uri_retrieve(
        self,
        output_file_object,
        study_instance_uid,
        series_instance_uid,
        sop_instance_uid,
    ):
        self.download(
            output_file_object,
            f"{self._arc_wado_url}/wado?requestType=WADO&"
            f"studyUID={study_instance_uid}&seriesUID={series_instance_uid}&objectUID={sop_instance_uid}",
        )

    def retrieve_multiple(self, output_file_object, level, object_uids):
        self.download(
            output_file_object,
            f"{self._arc_wado_url}/rs/multiple?accept=application/zip&level={level}"
            f"&objectUID={'&objectUID='.join(object_uids)}",
        )

    def download(self, output_file_object, url):
        resp = self._process_request(requests.get, url, stream=True)
        if resp.status_code == 200:
            resp.raw.decode_content = True
            shutil.copyfileobj(resp.raw, output_file_object)

    def add_device(self, aet, host=None, port=None):
        if host is None:
            host = f"{socket.gethostname()}.{os.getenv("NETWORK_NAME", "dcm4chee")}"
        if port is None:
            port = int(os.environ.get("DICOM_PORT", "11112"))

        # add minimally configured device entry so that archive can establish DICOM connections
        # e.g. for storage commitment replies
        device = {
            "dicomDeviceName": aet,
            "dicomDescription": "client device",
            "dicomInstalled": True,
            "dicomNetworkConnection": [
                {
                    "cn": "dicom",
                    "dicomHostname": host,
                    "dicomPort": port,
                }
            ],
            "dicomNetworkAE": [
                {
                    "dicomAETitle": aet,
                    "dicomNetworkConnectionReference": ["/dicomNetworkConnection/0"],
                }
            ],
        }

        resp = self._process_request(requests.get,f"{self.arc_url}/devices/{aet}")

        if resp.status_code == 404:
            method = requests.post
        elif resp.status_code == 200:
            method = requests.put
        else:
            raise RuntimeError("Unknown response when querying for client device")

        headers = {"Content-Type": "application/json"}

        resp = self._process_request(method,f"{self.arc_url}/devices/{aet}", headers=headers, data=json.dumps(device))

        if resp.status_code != 204:
            raise RuntimeError(f"Reason: {resp.reason} Code: {resp.status_code}")

        return True

    def add_exporter(
        self,
        ae_title,
        arc_ae_title="DCM4CHEE",
        export_as_source_ae=True,
        exporter_id="test_exporter",
    ):
        def deep_update(source, overrides):
            """
            Update a nested dictionary or similar mapping.
            Modify ``source`` in place.
            """
            for key, value in overrides.items():
                if isinstance(value, collections.abc.Mapping) and value:
                    returned = deep_update(source.get(key, {}), value)
                    source[key] = returned
                else:
                    source[key] = overrides[key]
            return source

        exporter = {
            "dcmDevice": {
                "dcmArchiveDevice": {
                    "dcmExporter": [
                        {
                            "dcmExporterID": exporter_id,
                            "dcmURI": f"dicom:{ae_title}",
                            "dcmQueueName": "Export",
                            "dicomAETitle": arc_ae_title,
                            "dcmExportAsSourceAE": export_as_source_ae,
                        }
                    ]
                }
            }
        }

        resp = self._process_request(requests.get, f"{self.arc_url}/devices/dcm4chee-arc")

        arc_device = resp.json()
        arc_device = deep_update(arc_device, exporter)

        resp = self._process_request(requests.put, f"{self.arc_url}/devices/dcm4chee-arc",
                                     headers={"Content-Type": "application/json"},
                                     data=json.dumps(arc_device))

        if resp.status_code != 204:
            raise RuntimeError(f"Reason: {resp.reason} Code: {resp.status_code}")
        return True

    def export_study(self, exporter_id, study_iuid):
        export_url = f"{self._arc_wado_url}/rs/studies/{study_iuid}/export/{exporter_id}"
        resp = self._process_request(requests.post, export_url)
        return resp
