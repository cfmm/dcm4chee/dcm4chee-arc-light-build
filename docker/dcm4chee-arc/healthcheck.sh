#!/usr/bin/env bash
set -e
set -x

http_code=$(curl -s -o /dev/null -w "%{http_code}" http://127.0.0.1:${HTTP_PORT:-8080}/${ARCHIVE_DEVICE_NAME:-dcm4chee-arc}/ui2)
if [[ $http_code -eq 302 ]]; then
    exit 0
else
    exit 1
fi
