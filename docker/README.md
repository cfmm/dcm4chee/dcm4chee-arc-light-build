1. Export database from a production system running in an lxd container

```
lxc exec [remote:]{container} -- pg_dump -h localhost -p 5432 -U pacs -W pacs --format c > db.dump
```

2. Import the data into the database.
```
docker compose up db -d
docker exec -i dcm4chee-db-1 sh -c 'exec pg_restore -U pacs --dbname=pacs' < db.dump
```

3. Increasing docker space available.
The production database can be very large (10s to 100s of GB) and may exceed the resource available to docker by default.
On macOS, open the docker desktop dashboard and go to the preferences (gear icon in toolbar). 
Under Resources | Advanced, increase the virtual disk limit. You will need to restart docker for the changes to take place.
These limits are the total disk limit for all docker components, e.g. containers, build cache, volumes, and images.
