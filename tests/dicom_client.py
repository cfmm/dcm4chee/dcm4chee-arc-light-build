import datetime
import time
from argparse import Namespace
from typing import Literal

from pydicom import read_file, Dataset, uid
from pydicom.sequence import Sequence
from pynetdicom.apps.common import create_dataset
from pynetdicom._globals import ALL_TRANSFER_SYNTAXES
from pynetdicom.presentation import AllStoragePresentationContexts
from pynetdicom.sop_class import (
    StorageCommitmentPushModel,
    StorageCommitmentPushModelInstance,
    Verification,
)
from pynetdicom.status import GENERAL_STATUS

from pynetdicom import (
    AE,
    QueryRetrievePresentationContexts,
    StoragePresentationContexts,
    VerificationPresentationContexts,
    BasicWorklistManagementPresentationContexts,
    StorageCommitmentPresentationContexts,
    evt,
    build_role,
)
from pynetdicom.pdu_primitives import UserIdentityNegotiation

from pynetdicom.sop_class import (
    ModalityWorklistInformationFind,
    PatientRootQueryRetrieveInformationModelFind,
    StudyRootQueryRetrieveInformationModelFind,
    PatientStudyOnlyQueryRetrieveInformationModelFind,
    StudyRootQueryRetrieveInformationModelGet,
)

from pynetdicom.sop_class import (
    PatientRootQueryRetrieveInformationModelMove,
    StudyRootQueryRetrieveInformationModelMove,
    PatientStudyOnlyQueryRetrieveInformationModelMove,
)


class StorageCommitError(Exception):
    pass


class DicomClient(object):
    def __init__(
        self,
        host,
        port,
        called_aet="DCM4CHEE",
        calling_aet="CLIENT",
        calling_port=29867,
        tls_client_context=None,
    ):
        self.host = host
        self.port = port
        self.called_aet = called_aet
        self.calling_aet = calling_aet
        self.calling_port = calling_port
        self.username = None
        self.password = None
        self.tls_client_context = tls_client_context

    def as_user(self, username, password):
        self.username = username
        self.password = password

    def establish_association(
        self,
        retry=False,
        requested_contexts=None,
        ext_neg=None,
        evt_handlers=None,
        positive_response_requested=False,
        user_identity_type=2,
    ):
        # ensure the DICOM server is up and ready to establish associations
        assoc = self.attempt_negotiation(
            requested_contexts,
            ext_neg,
            evt_handlers,
            positive_response_requested,
            user_identity_type,
        )
        if retry:
            while not assoc.is_established:
                time.sleep(0.5)
                assoc = self.attempt_negotiation(
                    requested_contexts,
                    ext_neg,
                    evt_handlers,
                    positive_response_requested,
                    user_identity_type,
                )
        return assoc

    def attempt_negotiation(
        self,
        requested_contexts=None,
        ext_neg=None,
        evt_handlers=None,
        positive_response_requested=False,
        user_identity_type=2,
    ):
        ae = AE(self.calling_aet)

        # Disable timeouts
        # ae.dimse_timeout = None
        # ae.network_timeout = None

        ae.requested_contexts = requested_contexts

        if not ext_neg:
            ext_neg = list()

        if self.username or positive_response_requested > 2:
            # Add user identity negotiation request - passwords are sent in the clear!
            user_identity = UserIdentityNegotiation()
            user_identity.positive_response_requested = positive_response_requested
            user_identity.user_identity_type = user_identity_type
            if self.username:
                user_identity.primary_field = self.username.encode()
            if self.password:
                user_identity.secondary_field = self.password.encode()
            ext_neg.append(user_identity)

        tls_args = (self.tls_client_context, None) if self.tls_client_context else None

        return ae.associate(
            self.host,
            self.port,
            ae_title=self.called_aet,
            ext_neg=ext_neg,
            evt_handlers=evt_handlers,
            tls_args=tls_args,
        )

    def echo(self, positive_response_requested=False, user_identity_type=2):
        assoc = self.establish_association(
            requested_contexts=VerificationPresentationContexts,
            positive_response_requested=positive_response_requested,
            user_identity_type=user_identity_type,
        )

        if assoc.is_established:
            status = assoc.send_c_echo()

            assoc.release()

            return status

        return None

    def store(self, path):
        assoc = self.establish_association(
            requested_contexts=StoragePresentationContexts
        )

        if assoc.is_established:
            ds = read_file(path)

            # `status` is the response from the peer to the store request
            # but may be an empty pydicom Dataset if the peer timed out or
            # sent an invalid dataset.
            status = assoc.send_c_store(ds)

            assoc.release()

            return status, ds

        return None, None

    def query(self, positive_response_requested=False, **query_terms):
        query_model = StudyRootQueryRetrieveInformationModelFind

        identifier = Dataset()
        identifier.QueryRetrieveLevel = "STUDY"
        for item in ("StudyInstanceUID", "StudyDescription"):
            setattr(identifier, item, "")

        for item in query_terms:
            setattr(identifier, item, query_terms[item])
        retval = list()

        assoc = self.establish_association(
            positive_response_requested=positive_response_requested,
            requested_contexts=(
                QueryRetrievePresentationContexts
                + BasicWorklistManagementPresentationContexts
            ),
        )
        assoc_established = assoc.is_established
        if assoc_established:
            # Send query
            responses = assoc.send_c_find(identifier, query_model)
            for status, rsp_identifier in responses:
                if status and status.Status in [0xFF00, 0xFF01]:
                    retval.append(rsp_identifier)
        assoc.release()
        return retval

    def retrieve(self, sop_class_uid, positive_response_requested=False, **query_terms):
        query_model = StudyRootQueryRetrieveInformationModelGet

        identifier = Dataset()
        identifier.QueryRetrieveLevel = "STUDY"
        for item in ("StudyInstanceUID", "StudyDescription"):
            setattr(identifier, item, "")

        for item in query_terms:
            setattr(identifier, item, query_terms[item])

        # Extended Negotiation - SCP/SCU Role Selection
        ext_neg = []
        for cx in StoragePresentationContexts:
            # Add SCP/SCU Role Selection Negotiation to the extended negotiation
            # We want to act as a Storage SCP
            ext_neg.append(build_role(cx.abstract_syntax, scp_role=True))

        retval = list()

        def handle_store(event):
            ds = event.dataset
            retval.append(ds)
            return 0x0000

        assoc = self.establish_association(
            requested_contexts=[
                c
                for c in QueryRetrievePresentationContexts
                if c.abstract_syntax == StudyRootQueryRetrieveInformationModelGet
            ]
            + [
                c
                for c in StoragePresentationContexts
                if c.abstract_syntax == sop_class_uid
            ],
            ext_neg=[build_role(sop_class_uid, scp_role=True)],
            evt_handlers=[(evt.EVT_C_STORE, handle_store)],
            positive_response_requested=positive_response_requested,
        )
        assoc_established = assoc.is_established
        if assoc_established:
            # Send query
            responses = assoc.send_c_get(identifier, query_model)
            for status, retrieved_ds in responses:
                if not status:
                    print(
                        "Connection timed out, was aborted or received invalid response"
                    )
        assoc.release()
        return retval

    def store_scp(self, ae_title, handle_store):
        # Create application entity
        ae = AE()

        transfer_syntax = ALL_TRANSFER_SYNTAXES[:]
        store_handlers = [(evt.EVT_C_STORE, handle_store)]
        ae.ae_title = ae_title
        for cx in AllStoragePresentationContexts:
            ae.add_supported_context(cx.abstract_syntax, transfer_syntax)

        scp = ae.start_server(
            ("", self.calling_port), block=False, evt_handlers=store_handlers
        )
        return scp

    def move(
        self,
        matching_keys,
        query_retrieve_level: Literal[
            "PATIENT", "STUDY", "SERIES", "INSTANCE"
        ] = "PATIENT",
        query_model: Literal[
            StudyRootQueryRetrieveInformationModelMove,
            PatientStudyOnlyQueryRetrieveInformationModelMove,
            PatientRootQueryRetrieveInformationModelMove,
        ] = PatientRootQueryRetrieveInformationModelMove,
    ):
        store_ae_title = self.calling_aet
        move_ae_title = self.calling_aet
        query_ae_title = self.calling_aet

        matching_keys["QueryRetrieveLevel"] = query_retrieve_level

        # retrieve data using C-MOVE
        identifier = create_dataset(
            Namespace(
                file=False, keyword=[f"{k}={matching_keys[k]}" for k in matching_keys]
            )
        )

        # Start the Store SCP to receive the dataset

        retval = list()

        def handle_store(event):
            ds_ = event.dataset

            # Add the file meta information elements
            ds_.file_meta = event.file_meta

            retval.append(ds_)

            status_ds = Dataset()
            status_ds.Status = 0x0000

            return status_ds

        scp = self.store_scp(store_ae_title, handle_store)

        # Create application entity
        ae = AE()

        ae.ae_title = query_ae_title

        assoc = self.establish_association(
            requested_contexts=[c for c in QueryRetrievePresentationContexts],
        )

        if assoc.is_established:
            # Send query
            move_aet = move_ae_title
            assoc.send_c_move(identifier, move_aet, query_model)

            starttime = datetime.datetime.now()
            timeout = datetime.timedelta(seconds=0)
            while len(retval) < 1 and timeout < datetime.timedelta(seconds=5):
                time.sleep(1)
                timeout = datetime.datetime.now() - starttime

            assoc.release()
        if scp:
            scp.shutdown()
        return retval

    def storage_commit(self, sop_class_uid, sop_instance_uid):
        # get storage commitment result for an instance

        ds = Dataset()
        ds.TransactionUID = uid.generate_uid()

        # Referenced SOP Sequence
        refd_sop_sequence = Sequence()
        ds.ReferencedSOPSequence = refd_sop_sequence

        refd_sop1 = Dataset()
        refd_sop1.ReferencedSOPClassUID = sop_class_uid
        refd_sop1.ReferencedSOPInstanceUID = sop_instance_uid
        refd_sop_sequence.append(refd_sop1)

        retval = list()

        client_ae = AE(self.calling_aet)
        # Add the requested presentation contexts (Storage SCU)
        client_ae.add_supported_context(Verification, scp_role=True, scu_role=True)
        client_ae.add_supported_context(
            StorageCommitmentPushModel, scp_role=True, scu_role=True
        )

        def handle_report(event):
            ds_ = event.event_information
            retval.append(ds_)
            return 0x0000, None

        assoc = self.establish_association(
            requested_contexts=[c for c in StorageCommitmentPresentationContexts],
            ext_neg=[build_role(sop_class_uid, scu_role=True)],
        )
        client_ae.start_server(
            ("", self.calling_port),
            block=False,
            evt_handlers=((evt.EVT_N_EVENT_REPORT, handle_report),),
        )

        assoc.send_n_action(
            dataset=ds,
            action_type=1,
            class_uid=StorageCommitmentPushModel,
            instance_uid=StorageCommitmentPushModelInstance,
            msg_id=1,
            meta_uid=None,
        )
        starttime = datetime.datetime.now()
        timeout = datetime.timedelta(seconds=0)
        while len(retval) < 1 and timeout < datetime.timedelta(seconds=20):
            time.sleep(1)
            timeout = datetime.datetime.now() - starttime

        assoc.release()
        client_ae.shutdown()

        if len(retval) < 1:
            raise TimeoutError("Storage commit timeout")

        return retval

    def query_for_dataset(self, ds, positive_response_requested=False):
        return self.query(
            positive_response_requested=positive_response_requested,
            StudyInstanceUID=ds["StudyInstanceUID"].value,
            StudyDescription="",
        )

    def retrieve_dataset(self, ds, positive_response_requested=False):
        return self.retrieve(
            positive_response_requested=positive_response_requested,
            sop_class_uid=ds["SOPClassUID"].value,
            StudyInstanceUID=ds["StudyInstanceUID"].value,
            StudyDescription="",
        )

    def confirm_storage(self, ds):
        result = self.storage_commit(
            ds["SOPClassUID"].value, ds["SOPInstanceUID"].value
        )[0]
        if "FailedSOPSequence" in result:
            raise StorageCommitError(
                f"Storage commit for SOP IUID {ds['SOPInstanceUID'].value} failed "
                f"with code {result.FailedSOPSequence[0].FailureReason}: "
                f"{GENERAL_STATUS[result.FailedSOPSequence[0].FailureReason][1]}"
            )
        if "ReferencedSOPSequence" in result:
            for sequence_item in result.ReferencedSOPSequence:
                if sequence_item.ReferencedSOPInstanceUID == ds["SOPInstanceUID"].value:
                    return True
