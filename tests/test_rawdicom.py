import filecmp
import tempfile
from conftest import first_ds


def test_retrieve_zip(arc):
    ds = first_ds(arc.ds["physio1"])
    arc.rest.as_user("admin", "secret")
    with tempfile.NamedTemporaryFile() as temp:
        arc.rest.wado_uri_retrieve(
            output_file_object=temp,
            study_instance_uid=ds["StudyInstanceUID"].value,
            series_instance_uid=ds["SeriesInstanceUID"].value,
            sop_instance_uid=ds["SOPInstanceUID"].value,
        )
        assert filecmp.cmp(
            temp.name, "data/physio1.zip"
        ), "Invalid retrieved raw Dicom file payload"
