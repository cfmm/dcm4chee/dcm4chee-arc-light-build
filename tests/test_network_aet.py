import itertools


def test_connect_with_user_identity(dicom_tls_client, dicom_plaintext_client):
    # TLS:

    # fails without user identity
    dicom_tls_client.as_user(None, None)
    assert (
        dicom_tls_client.echo() is None
    ), "C-ECHO over DICOM TLS connection incorrectly succeeds w/o User Identity Negotiation"

    # fails without user identity if using Plaintext AET
    dicom_tls_client_called_aet = dicom_tls_client.called_aet
    dicom_tls_client.called_aet = dicom_plaintext_client.called_aet
    assert (
        dicom_tls_client.echo() is None
    ), "C-ECHO over DICOM TLS connection incorrectly succeeds w/o User Identity Negotiation (TLS AET)"
    dicom_tls_client.called_aet = dicom_tls_client_called_aet

    # fails with incorrect credentials
    dicom_tls_client.as_user("admin", "incorrect")
    assert (
        dicom_tls_client.echo() is None
    ), "C-ECHO over DICOM TLS connection incorrectly succeeds with incorrect credentials"

    # fails with incorrect password when positive response requested
    dicom_tls_client.as_user("admin", "incorrect")
    assert dicom_tls_client.echo(positive_response_requested=True) is None, (
        "C-ECHO over DICOM TLS connection incorrectly succeeds with incorrect credentials"
        "when positive response requested"
    )

    # fails with any user identity type other than 2 (username/password)
    dicom_tls_client.as_user("admin", "incorrect")

    for (
        user_identity,
        user_identity_type,
        positive_response_requested,
    ) in itertools.product(
        [("admin", "incorrect"), ("admin", None), (None, None)],
        [1, 3, 4, 5],
        [True, False],
    ):
        dicom_tls_client.as_user(user_identity[0], user_identity[1])
        assert (
            dicom_tls_client.echo(
                user_identity_type=user_identity_type,
                positive_response_requested=positive_response_requested,
            )
            is None
        ), (
            f"C-ECHO over DICOM TLS connection incorrectly succeeds with user_identity_type {user_identity_type},"
            f" positive_response_requested {positive_response_requested}"
            "when positive response requested"
        )

    # succeeds otherwise
    dicom_tls_client.called_aet
    dicom_tls_client.as_user("admin", "secret")
    assert (
        dicom_tls_client.echo() is not None
    ), "C-ECHO over DICOM TLS connection incorrectly fails with correct credentials"

    # Plaintext:

    # succeeds without user identity (since it's optional)
    dicom_plaintext_client.as_user(None, None)
    assert (
        dicom_plaintext_client.echo() is not None
    ), "C-ECHO over DICOM Plaintext connection incorrectly fails w/o User Identity Negotiation"

    # succeeds with user identity too
    dicom_tls_client.as_user("admin", "secret")
    assert (
        dicom_tls_client.echo() is not None
    ), "C-ECHO over DICOM Plaintext connection incorrectly fails with correct credentials"

    # fails when using the TLS port with a plaintext connection
    try:
        dicom_plaintext_client.port = 11112
        dicom_plaintext_client.as_user(None, None)
        assert (
            dicom_plaintext_client.echo() is None
        ), "C-ECHO over DICOM Plaintext connection incorrectly succeeds w/o User Identity Negotiation (using TLS port)"
    finally:
        dicom_plaintext_client.port = 11114
