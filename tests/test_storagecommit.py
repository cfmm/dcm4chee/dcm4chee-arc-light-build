import pytest

def test_storage_commit(arc, datasets):
    for dataset in datasets:
        ds = arc.ds[dataset]
        # Storage commitment is intended to work with associations that don't negotiate user identity
        for d in ds:
            assert arc.dicom.confirm_storage(
                ds[d]
            ), f"Storage commit (no user identity negotiation) failure for {d}"
