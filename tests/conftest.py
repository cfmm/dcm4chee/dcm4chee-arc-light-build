import glob
import warnings
from urllib3.exceptions import InsecureRequestWarning
from types import SimpleNamespace

from authlib.integrations.requests_client import OAuth2Session, OAuth2Auth
import pydicom
from pydicom.errors import InvalidDicomError
import pytest
import requests
import ssl
import os

from cfmm_keycloak.fixtures import (
    keycloak_service,
    samba_service,
)
from dcm4chee.fixtures import arc_config_service
from keycloak import KeycloakDeleteError
from cfmm_keycloak.keycloak import ConfigureKeycloak

from dicom_client import DicomClient
from rest_client import RESTClient

@pytest.fixture(scope="session")
def datasets():
    return (
    # for access control and storage commit
    "Principal1-Project1",
    "Principal1-Project2",
    "Principal2-Project1",
    "Principal1-UndefinedProject",
    "UndefinedPrincipal-UndefinedProject",
    # testing for invalid project under a valid principal, with a prefix of the project name being a valid project
    "Principal1-Project1Undefined",
    # for testing raw dicom handling
    "physio1",
    # appending '/' treats value as subdirectory and matches all files in it
    # 'raw-with-localizer/',
    "CFMM-Development-1/",
    "CFMM-Development-2/",
    "CFMM-Development-3/",
)

# parameters for incoming DICOM connections
calling_aet = "CLIENT"
calling_port = 29867
exporter_id = "pytests_exporter"


def pytest_addoption(parser):
    parser.addoption(
        "--skip_store_datasets", action="store_true", help="Skip send datasets to arc."
    )
    parser.addoption(
        "--skip_delete_datasets",
        action="store_true",
        help="Skip delete datasets from arc.",
    )


@pytest.fixture(scope="session")
def realm_config(samba_service):  # noqa F811
    config = samba_service.data

    config["ldap_url"] = f"ldap://{os.environ['SAMBA_HOST']}"
    config["roles"] = {
        "dcm4chee_access": os.getenv("AUTH_USER_ROLE", "arc-users"),
        "dcm4chee_reload": os.getenv("RELOAD_USER_ROLE", "arc-reload"),
        "dcm4chee_admin": os.getenv("SUPER_USER_ROLE", "arc-admin"),
        "datacare": os.getenv("DATACARE_USER_ROLE", "Datacare")
    }
    return config


@pytest.fixture(scope="session")
def arc_url():
    return f"http://{os.environ['ARCHIVE_HOST']}/{os.environ['ARCHIVE_DEVICE_NAME']}"


@pytest.fixture(scope="session")
def admin_auth():
    url = f"http://keycloak.dcm4chee/realms/cfmm/.well-known/openid-configuration"
    openid_configuration = requests.get(url).json()

    # Obtain an access token
    with OAuth2Session(
        client_id=os.environ["RS_CLIENT_ID"],
        token_endpoint=openid_configuration["token_endpoint"],
    ) as session:
        token = session.fetch_token(
            openid_configuration["token_endpoint"],
            username="admin",
            password="secret",
        )
        yield OAuth2Auth(token)


@pytest.fixture(scope="session")
def skip_store_datasets(request):
    return request.config.getoption("--skip_store_datasets", False)


@pytest.fixture(scope="session")
def skip_delete_datasets(request):
    return request.config.getoption("--skip_delete_datasets", False)


@pytest.fixture(scope="session")
def dicom_plaintext_client():
    return DicomClient(
        host=os.environ["ARCHIVE_HOST"],
        port=11114,
        calling_aet=calling_aet,
        called_aet=os.environ["AE_TITLE"],
        calling_port=calling_port,
    )


@pytest.fixture(scope="session")
def dicom_tls_client():
    ctx = ssl.create_default_context()
    ctx.check_hostname = False
    ctx.verify_mode = ssl.CERT_NONE
    ctx.set_ciphers("AES128-SHA")

    return DicomClient(
        host=os.environ["ARCHIVE_HOST"],
        port=11112,
        calling_aet=calling_aet,
        called_aet=f"{os.environ['AE_TITLE']}TLS",
        calling_port=calling_port,
        tls_client_context=ctx,
    )


@pytest.fixture(scope="session")
def rest_client(dicom_plaintext_client):
    return RESTClient(
        client_id=os.environ["RS_CLIENT_ID"],
        realm_name=os.environ["REALM_NAME"],
        aetitle=dicom_plaintext_client.called_aet,
    )


def first_ds(ds_dict):
    return next(iter(ds_dict.values()))


@pytest.fixture(scope="session", autouse=False, name="arc")
def _arc(
    samba_service,
    arc_config_service,
    keycloak_service,
    realm_config,
    arc_url,
    dicom_plaintext_client,
    dicom_tls_client,
    rest_client,
    skip_store_datasets,
    skip_delete_datasets,
    datasets,
):
    # configure samba
    samba_service.make_groups()

    # configure keycloak
    realm_name = os.environ["REALM_NAME"]
    username = os.environ["KEYCLOAK_ADMIN"]
    password = os.environ["KEYCLOAK_ADMIN_PASSWORD"]

    config = ConfigureKeycloak(
        keycloak_service,
        realm_name,
        realm_config,
        username=username,
        password=password,
        user_realm="master",
    )
    try:
        config.admin.delete_realm(realm_name)
    except KeycloakDeleteError:
        pass

    config.create_realm()

    arc_client_rs = os.environ["RS_CLIENT_ID"]
    arc_client_ui = os.environ["UI_CLIENT_ID"]
    config.make_clients(
        arc_url,
        arc_client_rs,
        arc_client_ui if arc_client_ui != arc_client_rs else None,
    )

    with warnings.catch_warnings():
        warnings.filterwarnings("ignore", category=InsecureRequestWarning)

        # configure the arc instance
        arc_config_service.ae_titles()
        arc_config_service.network(
            keycloak_uri=os.environ["AUTH_SERVER_URL"] + '/',
            realm_name=os.environ["REALM_NAME"],
        )
        arc_config_service.ui()
        arc_config_service.webapp()

        # configure access control IDs
        arc_config_service.access_control(samba=samba_service)

        rest_client.as_user("admin", "secret")

        # add device/networkAE node on archive for receiving storage commit and c-move results
        rest_client.add_device(calling_aet, port=calling_port)

        rest_client.add_exporter(
            ae_title=calling_aet,
            arc_ae_title=dicom_plaintext_client.called_aet,
            exporter_id=exporter_id,
        )
        rest_client.reload()

    ds = {}
    # upload datasets
    for dataset in datasets:
        ds[dataset] = dict()
        for dataset_file in glob.glob(f"data/{dataset + '.' if not dataset.endswith('/') else dataset}*"):
            try:
                this_ds = pydicom.read_file(dataset_file)
                ds[dataset][this_ds.SOPInstanceUID] = this_ds
            except InvalidDicomError:
                continue
            if not skip_store_datasets:
                status = dicom_plaintext_client.store(dataset_file)[0]
                if not status:
                    raise RuntimeError("Cannot store dataset")

    yield SimpleNamespace(
        dicom=dicom_plaintext_client,
        dicom_tls=dicom_tls_client,
        rest=rest_client,
        ds=ds,
    )

    # to clean up, delete uploaded datasets
    client = rest_client.client()
    if not skip_delete_datasets:
        with warnings.catch_warnings():
            warnings.filterwarnings("ignore", category=InsecureRequestWarning)
            for dataset in datasets:
                try:
                    client.delete_study(first_ds(ds[dataset]).StudyInstanceUID)
                except StopIteration:
                    # ds could be empty
                    pass
                except requests.exceptions.HTTPError as e:
                    if e.response.status_code != 404:
                        raise
