def test_arc(arc_config_service, samba_service):
    keycloak_uri = "http://bogus.testing"
    realm_name = "ARC_Test_Realm"
    arc_config_service.ae_titles()
    arc_config_service.network(keycloak_uri, realm_name)
    arc_config_service.ui()
    arc_config_service.webapp()
    arc_config_service.access_control(samba_service)

    # should not fail when configured again
    arc_config_service.ae_titles()
    arc_config_service.network(keycloak_uri, realm_name)
    arc_config_service.ui()
    arc_config_service.webapp()
    arc_config_service.access_control(samba_service)
