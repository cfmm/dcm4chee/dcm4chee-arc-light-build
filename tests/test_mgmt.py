import requests


def test_access_no_auth(arc_url):
    resp = requests.get(
        f"{arc_url}/mgmt/access",
        verify=False,
    )

    assert resp.status_code == 401, "Unauthorized acces to mgmt credentials"


def test_access_auth(arc_url, admin_auth):
    resp = requests.get(f"{arc_url}/mgmt/access", verify=False, auth=admin_auth)
    assert resp.status_code == 200
    data = resp.json()
    assert "reason" not in data, data["reason"]

    assert data["ldapUrl"] == "ldap://ldap.dcm4chee:389"
    assert data["userDN"] == "cn=admin,dc=example,dc=com"
    assert data["userPassword"] == "secret"
    assert (
        data["deviceDN"]
        == "dicomDeviceName=dcm4chee-arc,cn=Devices,cn=DICOM Configuration,dc=example,dc=com"
    )
    assert data["devicesDN"] == "cn=Devices,cn=DICOM Configuration,dc=example,dc=com"
    assert data["configurationDN"] == "cn=DICOM Configuration,dc=example,dc=com"
    assert (
        data["aetsRegistryDN"]
        == "cn=Unique AE Titles Registry,cn=DICOM Configuration,dc=example,dc=com"
    )
