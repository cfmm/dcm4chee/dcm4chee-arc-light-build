import tempfile
import zipfile
from pathlib import Path

import pytest
import pydicom
from pydicom.errors import InvalidDicomError


@pytest.mark.parametrize(
    "level,object_uids,result_iuids",
    [
        (
            "study",
            [
                # "CFMM-Development-1/"
                "1.3.12.2.1107.5.2.0.18932.30000020012414045215400000016",
            ],
            [
                "1.3.12.2.1107.5.2.0.18932.30000020012413074372400000154",
                "1.3.12.2.1107.5.2.0.18932.30000020012413074372400000149",
                "1.3.12.2.1107.5.2.0.18932.30000020012413074372400000164",
            ],
        ),
        (
            "series",
            [
                # "CFMM-Development-3 series 1"
                "1.3.12.2.1107.5.2.0.18932.2020011511415084447000948.0.0.0",
            ],
            [
                "1.3.12.2.1107.5.2.0.18932.2020011511420147533000955",
                "1.3.12.2.1107.5.2.0.18932.2020011511420588091900958",
                "1.3.12.2.1107.5.2.0.18932.202001151141577584900952",
            ],
        ),
        (
            "instance",
            [
                # "CFMM-Development-3 series 1 instance 1"
                "1.3.12.2.1107.5.2.0.18932.202001151141577584900952",
            ],
            [
                "1.3.12.2.1107.5.2.0.18932.202001151141577584900952",
            ],
        ),
        (
            "study",
            [
                # "CFMM-Development-1/"
                "1.3.12.2.1107.5.2.0.18932.30000020012414045215400000016",
                # "CFMM-Development-2/"
                "1.3.12.2.1107.5.2.0.18932.30000020011714362748400000016",
            ],
            [
                "1.3.12.2.1107.5.2.0.18932.30000020011713121659400000144",
                "1.3.12.2.1107.5.2.0.18932.30000020011713121659400000139",
                "1.3.12.2.1107.5.2.0.18932.30000020012413074372400000154",
                "1.3.12.2.1107.5.2.0.18932.30000020012413074372400000149",
                "1.3.12.2.1107.5.2.0.18932.30000020012413074372400000164",
            ],
        ),
        (
            "series",
            [
                # "CFMM-Development-3 series 1"
                "1.3.12.2.1107.5.2.0.18932.2020011511415084447000948.0.0.0",
                # "CFMM-Development-1 series 6"
                "1.3.12.2.1107.5.2.0.18932.30000020012413074372400000152",
            ],
            [
                "1.3.12.2.1107.5.2.0.18932.30000020012413074372400000149",
                "1.3.12.2.1107.5.2.0.18932.2020011511420147533000955",
                "1.3.12.2.1107.5.2.0.18932.2020011511420588091900958",
                "1.3.12.2.1107.5.2.0.18932.202001151141577584900952",
            ],
        ),
        (
            "instance",
            [
                # "CFMM-Development-3 series 1 instance 3"
                "1.3.12.2.1107.5.2.0.18932.2020011511420147533000955",
                # "CFMM-Development-1 series 12 instance 1"
                "1.3.12.2.1107.5.2.0.18932.30000020012413074372400000164",
            ],
            [
                "1.3.12.2.1107.5.2.0.18932.30000020012413074372400000164",
                "1.3.12.2.1107.5.2.0.18932.2020011511420147533000955",
            ],
        ),
    ],
)
def test_download_multiple(level, object_uids, result_iuids, arc):
    arc.rest.as_user("admin", "secret")
    with tempfile.NamedTemporaryFile() as temp:
        arc.rest.retrieve_multiple(
            output_file_object=temp, level=level, object_uids=object_uids
        )
        with tempfile.TemporaryDirectory() as tmpdir, zipfile.ZipFile(
            temp.name, "r"
        ) as zp:
            zp.extractall(tmpdir)
            zip_sop_iuids = []
            for zip_file in Path(tmpdir).rglob("*"):
                try:
                    zip_ds = pydicom.read_file(zip_file)
                except (InvalidDicomError, IsADirectoryError):
                    continue
                zip_sop_iuids.append(zip_ds.SOPInstanceUID)
            assert set(zip_sop_iuids) == set(
                result_iuids
            ), f"Contents of downloaded zip file ({zip_sop_iuids}) don't match reference ({result_iuids})."
