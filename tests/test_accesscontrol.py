import pydicom
import pytest
import requests
import warnings
import os
import psycopg
from urllib3.exceptions import InsecureRequestWarning
from conftest import first_ds


def dataset_test(
    arc,
    dataset,
    remote_ds_fcn,
    attributes,
    action,
    should_succeed=False,
    access_type="DICOM",
):
    ds = first_ds(arc.ds[dataset])
    return_ds = remote_ds_fcn(ds)
    assert (
        not return_ds
        or type(return_ds) is pydicom.Dataset
        or len(return_ds) <= len(arc.ds[dataset])
    ), "Got too many matching datasets"
    if return_ds and type(return_ds) is list:
        return_ds = return_ds[0]
    if should_succeed:
        assert (
            return_ds
        ), f"{access_type}: {action} for dataset {ds.StudyDescription} by user {arc.dicom_tls.username} incorrectly fails"

        for attribute in attributes:
            assert getattr(return_ds, attribute) == getattr(ds, attribute), (
                f"{access_type}: Attribute {attribute} does not match "
                f"for dataset {ds.StudyDescription}, user {arc.dicom_tls.username}"
            )
    else:
        assert (
            not return_ds
        ), f"{access_type}: {action} for dataset {ds.StudyDescription} by user {arc.dicom_tls.username} incorrectly succeeds"


def test_password_auth(arc, datasets):
    ds = first_ds(arc.ds[datasets[0]])

    # ensure valid auth attempt succeeds
    arc.dicom_tls.as_user("admin", "secret")
    return_ds = arc.dicom_tls.query_for_dataset(ds)
    assert (
        return_ds
        and type(return_ds) is list
        and len(return_ds) > 0
        and type(return_ds[0]) is pydicom.Dataset
        and return_ds[0].StudyInstanceUID == ds.StudyInstanceUID
    ), "Cannot authenticate with dicom server"

    # ensure an incorrect password auth attempt fails
    arc.dicom_tls.as_user("admin", "incorrect")
    return_ds = arc.dicom_tls.query_for_dataset(ds)
    assert not return_ds, "Dicom server authenticates with invalid password"

    # ensure valid auth attempt succeeds (REST)
    client = arc.rest.client("admin", "secret")
    with warnings.catch_warnings():
        warnings.filterwarnings("ignore", category=InsecureRequestWarning)
        return_ds = pydicom.Dataset.from_json(
            client.retrieve_study_metadata(ds.StudyInstanceUID)[0],
            bulk_data_uri_handler=lambda x: None,
        )
    assert (
        return_ds
        and type(return_ds) is pydicom.Dataset
        and return_ds.StudyInstanceUID == ds.StudyInstanceUID
    ), "Cannot authenticate with REST service"


def test_valid_user_no_access(arc, datasets):
    ds = first_ds(arc.ds[datasets[0]])
    arc.dicom_tls.as_user("orphan", "secret")

    # Ensure valid noaccess user can authenticate but not find any datasets
    with warnings.catch_warnings():
        warnings.filterwarnings("ignore", category=InsecureRequestWarning)
        return_ds = arc.dicom_tls.query_for_dataset(ds)
    assert (
        not return_ds
    ), "Dicom server returns results for valid user with no relevant accessControlIDs"

    # same, REST
    client = arc.rest.client("orphan", "secret")
    with pytest.raises(requests.exceptions.HTTPError) as e:
        with warnings.catch_warnings():
            warnings.filterwarnings("ignore", category=InsecureRequestWarning)
            _ = pydicom.Dataset.from_json(
                client.retrieve_study_metadata(ds.StudyInstanceUID)[0],
                bulk_data_uri_handler=lambda x: None,
            )
    assert (
        e.value.response.status_code == 403
    ), "Did not get expected 403 status when querying as orphan user"


def test_auth_caching(arc, datasets):
    # ensure there's no caching of tokens server-side
    arc.dicom_tls.as_user("admin", "secret")
    dataset = datasets[0]
    dataset_test(
        arc,
        dataset,
        arc.dicom_tls.query_for_dataset,
        ("StudyInstanceUID",),
        "query",
        True,
    )
    arc.dicom_tls.as_user("user", "secret")
    dataset_test(
        arc,
        dataset,
        arc.dicom_tls.query_for_dataset,
        ("StudyInstanceUID",),
        "query",
        False,
    )


@pytest.mark.parametrize(
    "user,should_succeed",
    [
        # admin can query for all datasets
        (
            "admin",
            {
                "Principal1-Project1": True,
                "Principal1-Project1Undefined": True,
                "Principal1-Project2": True,
                "Principal2-Project1": True,
                "Principal1-UndefinedProject": True,
                "UndefinedPrincipal-UndefinedProject": True,
                "physio1": True,
                # 'raw-with-localizer/': True,
                "CFMM-Development-1/": True,
                "CFMM-Development-2/": True,
                "CFMM-Development-3/": True,
            },
        ),
        # datacare can query for all datasets
        (
            "datacare",
            {
                "Principal1-Project1": True,
                "Principal1-Project1Undefined": True,
                "Principal1-Project2": True,
                "Principal2-Project1": True,
                "Principal1-UndefinedProject": True,
                "UndefinedPrincipal-UndefinedProject": True,
                "physio1": True,
                # 'raw-with-localizer/': True,
                "CFMM-Development-1/": True,
                "CFMM-Development-2/": True,
                "CFMM-Development-3/": True,
            },
        ),
        # user cannot query for any datasets, except those with '*' accessControlIDs
        (
            "user",
            {
                "Principal1-Project1": False,
                "Principal1-Project1Undefined": False,
                "Principal1-Project2": False,
                "Principal2-Project1": False,
                "Principal1-UndefinedProject": False,
                "UndefinedPrincipal-UndefinedProject": False,
                "physio1": False,
                # 'raw-with-localizer/': False,
                "CFMM-Development-1/": False,
                "CFMM-Development-2/": False,
                "CFMM-Development-3/": False,
            },
        ),
        # principal1 should see 3 Principal1 projects
        (
            "principal1",
            {
                "Principal1-Project1": True,
                "Principal1-Project1Undefined": True,
                "Principal1-Project2": True,
                "Principal2-Project1": False,
                "Principal1-UndefinedProject": True,
                "UndefinedPrincipal-UndefinedProject": False,
                "physio1": False,
                # 'raw-with-localizer/': False,
                "CFMM-Development-1/": False,
                "CFMM-Development-2/": False,
                "CFMM-Development-3/": False,
            },
        ),
        # principal2 should see 1 Principal2 project
        (
            "principal2",
            {
                "Principal1-Project1": False,
                "Principal1-Project1Undefined": False,
                "Principal1-Project2": False,
                "Principal2-Project1": True,
                "Principal1-UndefinedProject": False,
                "UndefinedPrincipal-UndefinedProject": False,
                "physio1": False,
                # 'raw-with-localizer/': False,
                "CFMM-Development-1/": False,
                "CFMM-Development-2/": False,
                "CFMM-Development-3/": False,
            },
        ),
        # projectmember should see Principal1^Project1 only
        (
            "projectmember",
            {
                "Principal1-Project1": True,
                "Principal1-Project1Undefined": False,
                "Principal1-Project2": False,
                "Principal2-Project1": False,
                "Principal1-UndefinedProject": False,
                "UndefinedPrincipal-UndefinedProject": False,
                "physio1": False,
                # 'raw-with-localizer/': False,
                "CFMM-Development-1/": False,
                "CFMM-Development-2/": False,
                "CFMM-Development-3/": False,
            },
        ),
    ],
)
def test_query_retrieve_access(user, should_succeed, arc):
    username, password = user, "secret"

    with warnings.catch_warnings():
        warnings.filterwarnings("ignore", category=InsecureRequestWarning)
        arc.dicom_tls.as_user(username, password)
    arc.rest.as_user(username, password)

    def rest_query(ds):
        client = arc.rest.client()
        with warnings.catch_warnings():
            warnings.filterwarnings("ignore", category=InsecureRequestWarning)
            try:
                return pydicom.Dataset.from_json(
                    client.retrieve_study_metadata(ds.StudyInstanceUID)[0],
                    bulk_data_uri_handler=lambda x: None,
                )
            except requests.exceptions.HTTPError as e:
                if e.response.status_code == 404 or e.response:
                    return None
                else:
                    raise

    # perform dicom and rest queries for each study separately
    with warnings.catch_warnings():
        warnings.filterwarnings("ignore", category=InsecureRequestWarning)
        for dataset in should_succeed:
            dataset_test(
                arc,
                dataset,
                arc.dicom_tls.query_for_dataset,
                ("StudyInstanceUID",),
                "query",
                should_succeed[dataset],
            )
            dataset_test(
                arc,
                dataset,
                rest_query,
                ("StudyInstanceUID",),
                "query",
                should_succeed[dataset],
                "REST",
            )

    def validate_studies(studies_):
        should_find_studies = set(
            first_ds(arc.ds[study])["StudyInstanceUID"].value
            for study in arc.ds
            if should_succeed[study]
        )
        found_studies = set(study["StudyInstanceUID"].value for study in studies_)
        assert (
            should_find_studies == found_studies
        ), "Server did not list the expected set of studies"

    # perform a dicom query for all studies
    with warnings.catch_warnings():
        warnings.filterwarnings("ignore", category=InsecureRequestWarning)
        studies = arc.dicom_tls.query()
    validate_studies(studies)

    # perform a rest query for all studies
    with warnings.catch_warnings():
        warnings.filterwarnings("ignore", category=InsecureRequestWarning)
        studies = [
            pydicom.Dataset.from_json(ds, bulk_data_uri_handler=lambda x: None) for ds in arc.rest.client().search_for_studies()
        ]
    validate_studies(studies)

    def rest_retrieve(ds):
        client = arc.rest.client()
        with warnings.catch_warnings():
            warnings.filterwarnings("ignore", category=InsecureRequestWarning)
            try:
                return client.retrieve_study(ds.StudyInstanceUID)
            except requests.exceptions.HTTPError as e:
                if e.response.status_code == 404:
                    return None
                else:
                    raise

    # try to retrieve datasets
    for dataset in should_succeed:
        dataset_test(
            arc,
            dataset,
            arc.dicom_tls.retrieve_dataset,
            ("StudyInstanceUID", "PatientBirthDate"),
            "query",
            should_succeed[dataset],
        )
        dataset_test(
            arc,
            dataset,
            rest_retrieve,
            ("StudyInstanceUID", "PatientBirthDate"),
            "query",
            should_succeed[dataset],
            "REST",
        )


def test_accesscontrol_id(arc, samba_service, datasets):
    database = os.getenv('POSTGRES_DB')
    database_user = os.getenv('POSTGRES_USER')
    database_password = os.getenv('POSTGRES_PASSWORD')
    with psycopg.connect(dbname=database, user=database_user, password=database_password, host="db", port=5432) as connection:

        cursor = connection.cursor()

        cursor.execute("SELECT * FROM study;")

        # Fetch all rows from database
        records = cursor.fetchall()

    datacare = samba_service.find_group("Datacare")
    if datacare:
        datacare = datacare["gidNumber"]
    for record in records:
        study_description = record[26]
        access_control_id = record[1]
        try:
            principal, project = study_description.split("^", 1)
            group = samba_service.find_group(f"{principal}-{project}")
            if group and group.get('gidNumber', -1) == int(access_control_id):
                break
            group = samba_service.find_group(principal)
            if group and group.get('gidNumber', -1) == int(access_control_id):
                break
        except ValueError:
            "Study Description did not contain '^'"
            pass

        assert int(access_control_id) == datacare, "No study match not assigned to datacare"
