import pytest

from pynetdicom.sop_class import (
    PatientRootQueryRetrieveInformationModelMove,
    StudyRootQueryRetrieveInformationModelMove,
    PatientStudyOnlyQueryRetrieveInformationModelMove,
    uid_to_service_class,
)


@pytest.mark.parametrize(
    "dataset",
    (
        # for access control
        "Principal1-Project1",
        "Principal1-UndefinedProject",
        "UndefinedPrincipal-UndefinedProject",
        # for testing raw dicom handling
        "physio1",
    ),
)
@pytest.mark.parametrize(
    "query_model",
    [
        StudyRootQueryRetrieveInformationModelMove,
        PatientStudyOnlyQueryRetrieveInformationModelMove,
        PatientRootQueryRetrieveInformationModelMove,
    ],
)
def test_c_move_without_user_identity(arc, dataset, query_model):
    ds = list(arc.ds[dataset].values())[0]
    arc.dicom.as_user(None, None)

    if query_model == StudyRootQueryRetrieveInformationModelMove:
        matching_keys = dict(StudyInstanceUID=ds.StudyInstanceUID)
        query_retrieve_level = "STUDY"
    else:
        matching_keys = dict(PatientID=ds.PatientID)
        query_retrieve_level = "PATIENT"

    result = arc.dicom.move(
        matching_keys=matching_keys,
        query_retrieve_level=query_retrieve_level,
        query_model=query_model,
    )

    assert result, (
        f"C-MOVE (query model: {uid_to_service_class(query_model).__name__}) "
        f"did not result in receiving a dataset"
    )
    assert result[0].SOPInstanceUID == ds.SOPInstanceUID, (
        f"C-MOVE (query model: {uid_to_service_class(query_model).__name__})"
        f"Incorrect SOP Instance UID"
    )
