import requests
from dicomweb_client.api import DICOMwebClient
from dcm4chee.clients.rest import ArcRESTClient


class RESTClient(ArcRESTClient):
    def __init__(
        self,
        client_id="dcm4chee-arc-rs",
        arc_url="https://arc.dcm4chee/dcm4chee-arc",
        keycloak_url="http://keycloak.dcm4chee/",
        realm_name="dcm4che",
        aetitle="DCM4CHEE",
        client_secret_key=None,
    ):
        super().__init__(arc_url, keycloak_url,client_id,realm_name,aetitle,client_secret_key)

    def client(self, username=None, password=None):
        if username is None or password is None:
            token = self._active_token
        else:
            token = self._keycloak_openid.token(username, password)
        session = requests.session()
        session.verify = False
        session.trust_env = False
        return DICOMwebClient(
            url=self._arc_wado_url + "/rs",
            session=session,
            headers={"Authorization": f"Bearer {token['access_token']}"},
        )
