#!/usr/bin/env bash
set -x

# set up apt to download docker
# (see https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository)
apt-get update
apt-get install \
    -y --no-install-recommends \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common

#Add Docker’s official GPG key:
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

# add docker repo
add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

apt-get update

apt-get install \
    -y --no-install-recommends \
    docker-ce \
    docker-ce-cli \
    containerd.io \
    python3-docker \
    python3-virtualenv

# override the systemd start command to enable a TCP socket for remote deployment
mkdir -p /etc/systemd/system/docker.service.d
chmod 0755 /etc/systemd/system/docker.service.d
printf "[Service]\nExecStart=\nExecStart=/usr/bin/dockerd -H fd:// -H tcp://0.0.0.0:4243 --containerd=/run/containerd/containerd.sock\n" > override.conf

# create a python virtual environment for the tests
virtualenv venv
