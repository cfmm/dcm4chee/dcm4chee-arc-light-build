import datetime
import json
import time

import pytest

from conftest import calling_aet, exporter_id
from pydicom import Dataset


@pytest.mark.parametrize(
    "user,should_succeed",
    [
        # admin can query for all datasets
        (
            "admin",
            {
                "Principal1-Project1": True,
                "UndefinedPrincipal-UndefinedProject": True,
            },
        ),
        # user cannot query for any datasets, except those with '*' accessControlIDs
        (
            "user",
            {
                "Principal1-Project1": False,
                "UndefinedPrincipal-UndefinedProject": False,
            },
        ),
    ],
)
def test_export_studies(arc, user, should_succeed):
    for dataset in should_succeed:
        assert_prefix = f"User {user}, Dataset {dataset}: "
        ds = list(arc.ds[dataset].values())[0]
        expected_instance_uids = [v.SOPInstanceUID for k, v in arc.ds[dataset].items()]

        start_time = datetime.datetime.now()

        def handle_store(event):
            ds_ = event.dataset

            sop_instance_uid = ds_.SOPInstanceUID
            assert sop_instance_uid in expected_instance_uids, (
                assert_prefix
                + f"Received an invalid dataset with SOPInstanceUID={sop_instance_uid}"
            )
            expected_instance_uids.remove(sop_instance_uid)

            # Reset timeout timer
            nonlocal start_time
            start_time = datetime.datetime.now()

            status_ds = Dataset()
            status_ds.Status = 0x0000

            return status_ds

        # Start up store scp and wait for incoming connections
        scp = arc.dicom.store_scp(calling_aet, handle_store)

        # Initiate an export via REST API
        arc.rest.as_user(user, "secret")
        response = arc.rest.export_study(
            exporter_id=exporter_id, study_iuid=ds.StudyInstanceUID
        )

        # Use a short timer which is reset on receipt of data
        timeout = datetime.timedelta(seconds=0)
        while len(expected_instance_uids) > 0 and timeout < datetime.timedelta(
            seconds=10
        ):
            time.sleep(0.5)
            timeout = datetime.datetime.now() - start_time

        scp.shutdown()

        if should_succeed[dataset]:
            assert response.status_code == 202, (
                assert_prefix + f"Failed to schedule export task. "
                f"Reason: {response.reason}, "
                f"'{json.loads(response.content)['errorMessage']}' "
                f"Code: {response.status_code}"
            )
            assert len(expected_instance_uids) == 0, (
                assert_prefix + "Did not receive exported dataset"
            )
        else:
            assert response.status_code == 404, (
                assert_prefix + f"Expected the export to fail with status 404. "
                f"Got status {response.status_code}"
            )
            assert expected_instance_uids == [
                v.SOPInstanceUID for k, v in arc.ds[dataset].items()
            ], (assert_prefix + "Should not have received exported dataset")
