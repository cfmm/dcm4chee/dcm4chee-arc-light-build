# Build Tests

1. Configure Remote Docker Compose python interpreter in Pycharm 
   1. PyCharm | Settings... 
   2. Preferences | Project: dcm4chee-arc-light-bu... | Python Interpreter
   3. Add Interpreter | On Docker Compose...
   4. Configuration files:
      5. docker/docker-compose.yml
      6. docker/docker-compose.python.yml
   7. Service: python
   8. Environment variables:
      9. DCM4CHEE_VERSION: 5.31.2

## Compiling changes to `dcm4chee-arc-light`

```bash
./scripts/build.sh
cd docker
docker compose cp dcm4chee-arc/deployments/. arc:/opt/wildfly/standalone/deployments
```

Wildfly will automatically reload, since copy destination above is mapped to the wildfly container in the Docker compose file.

After several such auto-reloads of deployment archives, the container may run out of memory, fail to load archives and become unresponsive. Restart the container if that happens.

## Logging of containers

```bash
cd docker
docker compose exec -it arc tail -f /opt/wildfly/standalone/log/server.log 
```
